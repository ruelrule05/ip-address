<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddIPAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ip'    =>  'required|ip|unique:i_p_addresses',
            'label' =>  'max:255'
        ];
    }

    public function messages()
    {
        return [
            'ip.unique' =>  'IP Address already exists.'
        ];
    }
}
