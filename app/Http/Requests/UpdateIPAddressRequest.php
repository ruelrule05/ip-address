<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateIPAddressRequest extends FormRequest
{
    private $ip_address;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->ip_address = $this->route('ip_address');

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ip'    =>  'ip|unique:i_p_addresses,ip,' . $this->ip_address->id,
            'label' =>  'max:255'
        ];
    }

    public function messages()
    {
        return [
            'ip.unique' =>  'IP Address already exists.'
        ];
    }
}
