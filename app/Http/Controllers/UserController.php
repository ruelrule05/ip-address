<?php

namespace App\Http\Controllers;

use Auth;
use App\Events\UserLoggedIn;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum')->except(['login']);
    }

    public function details()
    {
        return response()->json(['details' => Auth::user()]);
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        } else {
            event(new UserLoggedIn($user));
            
            return response()->json(['success' => true, 'token' => $user->createToken(Str::random())->plainTextToken, 'user' => $user]);
        }
    }

    public function logout(Request $request)
    {
        Auth::user()->tokens()->delete();

        return response()->json(['success' => true]);
    }
}
