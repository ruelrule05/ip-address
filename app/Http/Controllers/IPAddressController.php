<?php

namespace App\Http\Controllers;

use App\Events\IPAdded;
use App\Events\IPAddressUpdated;
use App\Models\IPAddress;
use Illuminate\Http\Request;
use App\Http\Requests\AddIPAddressRequest;
use App\Http\Requests\UpdateIPAddressRequest;

class IPAddressController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['ips' => IPAddress::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddIPAddressRequest $request)
    {
        $ipAddress = new IPAddress();

        $ipAddress->fill($request->all());

        if ($ipAddress->save())
        {
            event(new IPAdded($ipAddress));
            return response()->json(['success' => true, 'message' => 'IP Address successfully added.']);
        } else {
            return response()->json(['success' => false, 'message' => 'Failed to add IP Address.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\IPAddress  $iPAddress
     * @return \Illuminate\Http\Response
     */
    public function show(IPAddress $ip_address)
    {
        return response()->json(['ip' => $ip_address]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\IPAddress  $iPAddress
     * @return \Illuminate\Http\Response
     */
    public function edit(IPAddress $iPAddress)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\IPAddress  $iPAddress
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateIPAddressRequest $request, IPAddress $ip_address)
    {
        $oldIpAddress = clone $ip_address; //Need to clone so that old ip address value will be retained even after filling with the new values

        $ip_address->fill($request->only('label'));

        if ($ip_address->update())
        {
            event(new IPAddressUpdated($oldIpAddress, $ip_address));
            
            return response()->json(['success' => true, 'message' => 'IP Address successfully updated.']);
        } else {
            return response()->json(['success' => false, 'message' => 'Failed to updated IP Address.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\IPAddress  $iPAddress
     * @return \Illuminate\Http\Response
     */
    public function destroy(IPAddress $iPAddress)
    {
        //
    }
}
