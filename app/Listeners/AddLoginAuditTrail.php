<?php

namespace App\Listeners;

use Auth;
use Carbon\Carbon;
use App\Models\AuditTrail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddLoginAuditTrail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        AuditTrail::create([
            'user_id'       =>  $event->user->id,
            'description'   =>  'User logged in on ' . Carbon::now()->format('F d, Y h:i:s A')
        ]);
    }
}
