<?php

namespace App\Listeners;

use Auth;
use Carbon\Carbon;
use App\Models\AuditTrail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateIPAddedAuditTrail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        AuditTrail::create([
            'user_id'       =>  Auth::user()->id,
            'description'   =>  $event->ipAddress->ip . (strlen($event->ipAddress->label) > 0 ? ' (' . $event->ipAddress->label . ')' : '') . ' added on ' . Carbon::now()->format('F d, Y h:i:s A')
        ]);
    }
}
