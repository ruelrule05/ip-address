<?php

namespace App\Listeners;

use Auth;
use Carbon\Carbon;
use App\Models\AuditTrail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddIpAddressUpdatedAuditTrail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        AuditTrail::create([
            'user_id'       =>  Auth::user()->id,
            'description'   =>  $event->oldIpAddress->ip . (strlen($event->oldIpAddress->label) > 0 ? ' (' . $event->oldIpAddress->label . ')' : '') . ' was changed to ' . $event->newIpAddress->ip . (strlen($event->newIpAddress->label) > 0 ? ' (' . $event->newIpAddress->label . ')' : '') . ' on ' . Carbon::now()->format('F d, Y h:i:s A')
        ]);
    }
}
