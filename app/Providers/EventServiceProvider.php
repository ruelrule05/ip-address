<?php

namespace App\Providers;

use App\Events\IPAdded;
use App\Events\IPAddressUpdated;
use App\Events\UserLoggedIn;
use App\Listeners\AddIpAddressUpdatedAuditTrail;
use App\Listeners\AddLoginAuditTrail;
use App\Listeners\CreateIPAddedAuditTrail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        IPAdded::class => [
            CreateIPAddedAuditTrail::class
        ],
        IPAddressUpdated::class => [
            AddIpAddressUpdatedAuditTrail::class
        ],
        UserLoggedIn::class => [
            AddLoginAuditTrail::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
