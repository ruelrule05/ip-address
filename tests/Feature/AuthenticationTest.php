<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthenticationTest extends TestCase
{
    private $token;

    public function testUserCanLogin()
    {
        $response = $this->withHeaders([
            'Accept'    =>  'application/json'
        ])->post('/api/login', [
            'email'     =>  'ruelrule05@gmail.com',
            'password'  =>  '123Password_'
        ]);

        $response->assertStatus(200)
                    ->assertJsonFragment(['success' => true]);

        $this->token = $response->decodeResponseJson()['token'];
    }

    public function testUserCannotLoginWithIncorrectPassword()
    {
        $response = $this->withHeaders([
            'Accept'    =>  'application/json'
        ])->post('/api/login', [
            'email'     =>  'ruelrule05@gmail.com',
            'password'  =>  '123Password_Incorrect'
        ]);

        $response->assertStatus(422)
                    ->assertJsonFragment([
                        'errors'   =>  [
                            'email' =>  ['The provided credentials are incorrect.']
                        ]
                    ]);
    }

    public function testNotLoggedInUsersAreForbidden()
    {
        $response = $this->withHeader('Accept', 'application/json')
                        ->get('/api/user');

        $response->assertUnauthorized();
    }

    public function testCanGetUserDetailsWhenLoggedIn()
    {
        $response = $this->withHeader('Accept', 'application/json')
                        ->post('/api/login',[
                            'email'     =>  'ruelrule05@gmail.com',
                            'password'  =>  '123Password_'
                        ]);

        $response->assertJsonFragment(['success' => true]);

        $token = $response->decodeResponseJson()['token'];

        $response = $this->withHeaders([
            'Accept'        =>  'application/json',
            'Authorization' =>  'Bearer ' . $token
        ])->get('/api/user');

        $response->assertStatus(200)
                ->assertJsonPath('details.email', 'ruelrule05@gmail.com');
    }
}
