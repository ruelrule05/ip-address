<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Events\IPAdded;
use App\Models\IPAddress;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IPAddressTest extends TestCase
{
    use DatabaseTransactions;

    private $token;

    public function setup() : void
    {
        parent::setup();

        $response = $this->withHeaders([
            'Accept'    =>  'application/json'
        ])->post('/api/login', [
            'email'     =>  'ruelrule05@gmail.com',
            'password'  =>  '123Password_'
        ]);

        $token = $response->decodeResponseJson()['token'];

        $this->token = $token;
    }

    public function testAddNewIPAddress()
    {
        $response = $this->withHeaders([
            'Accept'        =>  'application/json',
            'Authorization' =>  'Bearer ' . $this->token
        ])->post('/api/ip-addresses', [
            'ip'    =>  '52.35.150.15',
            'label' =>  'EM System'
        ]);

        $response->assertJsonFragment([
            'success'   =>  true
        ]);
    }

    public function testFailWhenAddingExistingIPAddress()
    {
        $response = $this->withheaders([
            'Accept'        =>  'application/json',
            'Authorization' =>  'Bearer ' . $this->token
        ])->post('/api/ip-addresses', [
            'ip'    =>  '52.35.150.15',
            'label' =>  'First System'
        ]);

        $response = $this->withheaders([
            'Accept'        =>  'application/json',
            'Authorization' =>  'Bearer ' . $this->token
        ])->post('/api/ip-addresses', [
            'ip'    =>  '52.35.150.15',
            'label' =>  'Existing System'
        ]);

        $response->assertStatus(422);
    }

    public function testUpdateIPAddress()
    {
        $ipAddress = new IPAddress();
        $ipAddress->ip = '192.168.254.1';
        $ipAddress->label = 'My Instance';
        $ipAddress->save();

        $response = $this->withHeaders([
            'Accept'        =>  'application/json',
            'Authorization' =>  'Bearer ' . $this->token
        ])->post('/api/ip-addresses/' . $ipAddress->id, [
            'label'     =>  'My updated instance',
            '_method'   =>  'patch'
        ]);

        $response->assertJsonFragment(['success' => true]);

        $ipAddress = IPAddress::where('ip', '192.168.254.1')->first();

        $this->assertEquals('My updated instance', $ipAddress->label, 'IP Address updated.');
    }

    public function testFailOnAddingInvalidIPAddress()
    {
        $response = $this->withHeaders([
            'Accept'        =>  'application/json',
            'Authorization' =>  'Bearer ' . $this->token
        ])->post('/api/ip-addresses', [
            'ip'    =>  '192.168.1',
            'label' =>  'Invalid IP'
        ]);

        $response->assertJsonFragment([
            'errors'    =>  [
                'ip'    =>  ['The ip must be a valid IP address.']
            ]
        ]);
    }
}
