#RUNNING THE SERVER

```
cp .env.example .env
composer install
php artisan key:generate
```

Update database connection on .env


```
php artisan migrate --seed
php artisan serve
```

###UNIT TESTING

To run test, use the command below

```
php artisan test
```


#Problem

<strong><u>IP Address Management Solution</u></strong>

Task is to design and implement a web-based IP address management solution to allow us to record an IP address and comment on its assignment. For example, we might create an entry for 202.92.249.111, and label it gifts.ad-group.com.au. Or we might label it ‘Spare’, or ‘BFBC2 Server’. The objective is to build a simple web application to provide this functionality using the following guidelines. This web application will allow an authenticated user to perform the following tasks:

- Log in to the system and receive an authenticated token, with all subsequent steps requiring this authenticated token.
- Add a new IP address to the database and attach a small label/comment to it.
- Modify an IP address to change the label.
- View an audit log of which changes have been made.
- The database should record all IP addresses, associated comments/labels, and an audit trail of any additions or changes to the database. The data types, keys, and relationships of these database elements is your choice. Your only restriction is that the code must work, and must confirm to the following design goals.

- Adds/changes should be not allowed if users are not authenticated.
- An audit trail should be maintained for every login, addition or change
- IP addresses must be validated as acceptable before being entered into the database
- The system should easily support the addition of new capabilities in the future with minimal effort.
- You do not need to provide any capacity to delete records or change an IP address once entered, only its comment/label needs to be modified. Similarly, there is no need to provide account management functionality or the ability to modify/update the audit table.

#Requirements

- You can use your preferred tech stack, however PHP (Lumen/Laravel/Symfony) for the backend and Angular for the frontend are recommended.
- The Front-End should connect to the backend to request information. The backend should use a MySQL database to store and retrieve the relevant information.
- Please submit your work on a publicly accessible Git repository. A repository with only one commit is not acceptable. It will be considered a “FAIL”. Showing the development process is just as important as the task itself.
- Please include a README with installation and usage instructions.
- If possible, include some attempt at testing your code [BONUS].
- Please do not include vendor directories (e.g. node_modules) in the submission package.

#Assessment Criteria

- Submission meets requirements.
- Submission demonstrates some knowledge of application design.
- Separation of concerns.
- Simplicity.
- UI is easy to use and displays information clearly.
- Code demonstrates consistency and adherence to common standards.